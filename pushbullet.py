#!/bin/bash
curl https://api.pushbullet.com/api/pushes \
      -u $1: \
      -d type=note \
      -d title="$2" \
      -d body="$3" \
      -X POST
